package com.alinaeric.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
//        String[] names = {"Meisam", "Sarah", "Brad", "Madeline", "Tom"};
//        System.out.println(names[2]);
        // names[5] = "Jerry"; // java arrays ara immutable

        // To add the 6th element in the string array, follow the code. However, it is time- and resources-consuming.
//        String[] newNames = new String[6];
//        for (int i = 0; i < names.length; i++) {
//            newNames[i] = names[i];
//        }
//        newNames[5] = "Jerry";
//        System.out.println(newNames[5]);


        // JAVA COLLECTIONS: ARRAY

        // one way to define array lists
        ArrayList<String> names = new ArrayList<>();
        // List<String> students = new ArrayList<>(); // an interface

        // add() method - to add elements in an array
        names.add("Alina");
        names.add("Bob");

        // get() method - needs an 'index' parameter and returns the element
        System.out.println(names.get(0)); // Alina

        // size() method - returns the size/length of an array
        System.out.println(names.size()); // 2

        // clear() method - to remove all elements in an array
        // names.clear();
        // System.out.println(names.size()); // 0

        // remove() method - removes an element
        // names.remove("Alina");
        System.out.println(names.get(0));

        // contains() method - determines if the element is present in the array and returns a boolean
        System.out.println(names.contains("Bob"));

        //isEmpty() method - determines if an array is empty or not and returns a boolean
        // names.remove("Bob");
        System.out.println(names.isEmpty());

        // indexOf() method - determines the index of an element
        System.out.println(names.indexOf("Alina")); // if -1 is printed, it means that the index cannot be found

        for (int i = 0; i < names.size(); i++) {
            System.out.println(names.get(i));
        }

        // JAVA COLLECTIONS: MAPS

        Map<String, String> emailList = new HashMap<>();

        // put() method - adds elements to the map
        emailList.put("Eric", "eric.alina@gmail.com");
        emailList.put("Brad", "Brad@gmail.com");

        // get() method - needs a 'key' parameter and returns the valye
        System.out.println(emailList.get("Eric")); // eric.alina@gmail.com

        // size() method - returns the size of the map
        System.out.println(emailList.size()); // 2

        // remove() method - needs a 'key' parameter and removes a key-value pair
        emailList.remove("Brad");
        System.out.println(emailList.size()); // 1

        // containsKey() method - needs a 'key' parameter and returns a boolean
        System.out.println(emailList.containsKey("Brad")); // false

        System.out.println(emailList.containsValue("Brad@gmail.com")); // false

        // clear() method - clears the key-value pairs in the map
        // isEmpty() method - determines if the map is empty or not, and returns a boolean

        // values() method - returns all the values in the map
        System.out.println(emailList.values());
        //keySet() method  returns all the keys in the map
        System.out.println(emailList.keySet());

        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student("Meisam", 15));
        students.add(new Student("Sarah", 16));

        for (Student s : students) {
            System.out.println(s.getName());
        }
    }
}
