package com.alinaeric.oopchallenge;

public class Organ {
    private String name;
    private String medicalCondition;

    public Organ(String name, String medicalCondition) {
        this.name = name;
        this.medicalCondition = medicalCondition;
    }

    public void getDetails () {
        System.out.println("Name: " + this.getName());
        System.out.println("Medical Condition: " + this.getMedicalCondition());
    }

    public String getName() {
        return name;
    }

    public String getMedicalCondition() {
        return medicalCondition;
    }
}



/*
 * OBJECT-ORIENTED PROGRAMMING CHALLENGE
 * Imagine you are a doctor and you want to check on your patient.
 * Name: Tom
 * Age: 25
 * Choose an Organ:
 *      1. Left Eye
 *      2. Right Eye
 *      3. Heart
 *      4. Stomach
 *      5. Skin
 *      6. Quit
 *
 * If 1 is selected:
 * Name: Left Eye
 * Medical Condition: Short sighted
 * Color: Blue
 *      1. Close the Eye
 *
 * If 1 is selected:
 * Left Eye Closed
 *
 * ===================================
 * If 2 is selected:
 * Name: Right Eye
 * Medical Condition: Normal
 * Color: Blue
 *      1. Close the Eye
 * If don't want to close the eye, choose any number other than 1
 *
 *=====================================
 * If 3 is selected:
 * Name: Heart
 * Medical Condition: Normal
 * Heart rate = 65
 *      1. Change the heart rate
 * If 1 is selected:
 * Enter the new heart rate:
 * 75
 * Heart rate changed to 75
 *
 *=====================================
 * If 4 is selected:
 * Name: Stomach
 * Medical Condition: PUD
 * Need to be fed
 *      1. Digest
 *
 * If 1 is selected:
 * Digesting begin...
 *
 * =====================================
 * If 5 is selected:
 * Name: Skin
 * Medical Condition: Burned
 *
 * =====================================
 * If 6 is selected:
 * The application will close.
 *
 */