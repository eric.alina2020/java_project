package com.alinaeric.arrays;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // ARRAYS
//        String[] students = new String[5];
//        students[0] = "Sarah";
//        students[1] = "Eric";
//        students[2] = "Tom";
//        students[3] = "Brad";
//        students[4] = "Madeline";
//        students[5] = "Brian";
//
//        System.out.println(students[2]); // Tom
//
//        // another way to create arrays
//        String[] employees = {"Sarah", "Eric", "Tom", "Brad", "Madeline"};
//        int[] numbers = {1, 2, 3, 4, 5, 6};
//        for (int i = 0; i < numbers.length; i++) {
//            System.out.println(numbers[i]); // 1 2 3 4 5 6
//        }

        String[] names = {"Sarah", "Eric", "Tom", "Brad", "Madeline"};
        int[] numbers = {134546, 5465561, 8779845, 216541, 894845};

        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        System.out.println("Please enter a name: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        for (int i = 0; i < names.length; i++) {

            // here, use equals() method instead of == as it runs an error
            if (name.equals(names[i])) {
                System.out.println(numbers[i]);
            }
        }
    }
}
