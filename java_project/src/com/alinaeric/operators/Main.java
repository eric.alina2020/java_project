package com.alinaeric.operators;

public class Main {
    public static void main(String[] args) {

        // RELATIONAL OPERATORS - include <, >, ==, >=, <=, !=
        int a = 5;
        int b = 5;

        boolean answer1 = a < b;
        boolean answer2 = a == b;
        boolean answer3 = a >= b;
        boolean answer4 = a != b;
        System.out.println(answer1); // false
        System.out.println(answer2); // true
        System.out.println(answer3); // true
        System.out.println(answer4); // false

        // LOGICAL OPERATORS - include || (or), && (and)
        boolean answer5 = a==5 || b==2;
        boolean answer6 = a==5 && b==2;
        System.out.println(answer5); // true
        System.out.println(answer6); // false

        // CONDITIONAL STATEMENTS - include if, else if, else

        boolean answer7 = a > 3;

        if (answer7) {
            System.out.println("a is greater than 3");
        } else {
            System.out.println("a is less than 3");
        }

        if (a > 0) {
            System.out.println("A is positive.");
        } else if (a < 0) {
            System.out.println("A is negative.");
        } else {
            System.out.println("A is zero.");
        }

        switch (a) {
            case 1:
                System.out.println("a is one.");
                break;
            case 2:
                System.out.println("a is two.");
                break;
            case 3:
                System.out.println("a is three.");
                break;
            default:
                System.out.println("a is not one, two, or three");
                break;
        }
    }
}
