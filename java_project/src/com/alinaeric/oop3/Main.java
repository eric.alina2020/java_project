package com.alinaeric.oop3;

public class Main {
    // COMPOSITION IN OBJECT-ORIENTED PROGRAMMING
    public static void main(String[] args) {

//        // Engine engine = new Engine ("Renault", 8000);
//        Car mercedes = new Car ("Mercedes AMG", 2, "Silver", new Engine("Renault", 8000));
//        System.out.println(mercedes.getName());
//        Engine engine = mercedes.getEngine();
//
//        System.out.println("Engine Model: " + mercedes.getEngine().getModel());

        // The NULL - means nothing or no value
//        Car mercedes = null;
//        if (mercedes != null) {
//            mercedes.getName();
//        } else {
//            System.out.println("The car was null");
//        }

        // The FINAL - values in a final variable can't be changed
//        final int a = 5;
//        a = a + 1; // Error!

        // When declaring objects as final, you cannot change the whole instance of the object but you can change the properties of your object.
        final Engine engine = new Engine("Renault", 8000);
        // engine = new Engine("Renault", 7000); // Error!
        engine.setRpm(10000); // rpm property can be changed
    }
}
