package com.alinaeric.oop2;

// child class can be extended/inherited to another child class
public class Shepherd extends Dog {
    public Shepherd(String name, String color, int legs, boolean hasTail) {
        super(name, color, legs, hasTail);
    }
}
