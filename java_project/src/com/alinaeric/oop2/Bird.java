package com.alinaeric.oop2;

public class Bird extends Animal {

    private int wings;

    public Bird(String name, String color, int legs, boolean hasTail, int wings) {
        super(name, color, legs, hasTail);
        this.wings = wings;
    }

    // use Ctrl + O to override the eat behavior of Animal class (parent class)
    // another kind of POLYMORPHISM - two methods inside a parent and a child class
    @Override
    public void eat(String food) {
        System.out.println("Chewing " + food);
    }

    public void fly () {
        System.out.println(this.getName() + " is flying");
    }

    // another kind of POLYMORPHISM - using methods with the same name in different ways
    // Examples are fly() methods with different parameters such as speed and name
//    public void fly (int speed) {
//    }

//    public void fly (String name) {
//    }

    // getter
    public int getWings() {
        return wings;
    }

    // setter
    public void setWings(int wings) {
        this.wings = wings;
    }
}
