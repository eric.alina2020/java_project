package com.alinaeric.oop2;

public class Dog extends Animal {

    // constructor for Dog class - to generate, hit Alt + Insert
    public Dog(String name, String color, int legs, boolean hasTail) {
        super(name, color, legs, hasTail);
    }
}
