package com.alinaeric.java_project;

public class Hello {
    // type "psvm" and hit Tab
    public static void main(String[] args) {
        System.out.println("Hello World");
        System.out.print("Hello World");

        // JAVA DATA TYPES AND VARIABLES
        int number = -5;
        System.out.println(number); // -5

        // long can store large numbers up to 2^63 in device RAM
        // compared to int that can store up to 2^31
        long myLong = 5;
        System.out.println(myLong); // 5

        // double occupies more space in device RAM than float
        double myDouble = 4.5;
        System.out.println(myDouble); // 4.5

        // store smaller numbers compared to double
        float myFloat = (float) 4.5;
        System.out.println(myFloat); // 4.5

        char myChar1 = 'M'; // can only accept single character
        System.out.println(myChar1); // M

        char myChar2 = '\u00A9';
        System.out.println(myChar2); // copyright symbol ©

        // String is a class, not a primitive data type
        String name = "Eric";
        System.out.println(name); // Eric

        boolean myBoolean = false;
        System.out.println(myBoolean); // false

        // ARITHMETIC OPERATORS - include +, -, *, /, % (remainder)
        int a = 5;
        int b = 2;
        int answer1 = a + b;
        int answer2 = a - b;
        int answer3 = a * b;
        int answer4 = a % b;
        int answer5 = a / b;
        double answer6 = (double) a / b;
        double answer7 = (double) a + b;

        System.out.println(answer1); // 7
        System.out.println(answer2); // 3
        System.out.println(answer3); // 10
        System.out.println(answer4); // 1
        System.out.println(answer5); // 2
        System.out.println(answer6); // 2.5
        System.out.println(answer7); // 7.0

        String firstName = "Emma ";
        String lastName = "Watson";
        String fullName = firstName + lastName; // concatenation
        System.out.println(fullName); // Emma Watson

        int score = 5;
        score += 2; // adds a specific value to the current value
        score -= 2; // subtracts a specific value to the current value
        score++; // increases the value by 1
        score--; // decreases the value by 1
        score /= 2;
        System.out.println(score); // 2
    }
}