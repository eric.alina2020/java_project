package com.alinaeric.loops;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // LOOPS = include for loops, while loops, and do loops

//        int a = 5;
//        for (int i = 0; i < a; i++) {
//            System.out.println(i);
//        }

//        while (a < 10) {
//
//            a++;
//            if (a == 8) {
//                continue; // means the program will run the next record of the while loop
//            }
//
//            System.out.println("Hello");
//            // break; // breaks the while loop and prints one "Hello" only
//            // a++; // by updating the value, infinite while loop stops!
//        }

        // do loops execute codes at least once no matter what the condition is
//        do {
//            System.out.println("Hello");
//        } while (a < 5);
//
//        // To get the user input:
//        System.out.println("Please enter a number: ");
//        Scanner scanner = new Scanner(System.in);
//        int answer = scanner.nextInt();
//        System.out.println("Answer was: " + answer);
//        System.out.println("Enter your name: ");
//        String name = scanner.next();
//        System.out.println("Hello " + name);
//
//        Random random = new Random();
//        int number = random.nextInt(20) + 1;
//        System.out.println("number: " + number);

        // CHALLENGE:
//        Create a game:
//        The concept is: Generate a random number and ask the user to guess the number. Continue asking until you receive the number. After 5 times of guessing, show a game over message.
//        TASK:
//        Show a welcome message. Ask the user's name and say hello to the user. Ask if we should start the game. If "Yes", generate a random number and ask for the user's guess, otherwise end the game. If the guess is correct, show a congratulation message and quit the game. If the number is incorrect, ask again until you receive the correct answer. As a hint, beside the first time, every time that you are asking for a number, tell the user to guess higher or lower. If the user failed 5 times, show a "Game Over" message and quit the game.

        System.out.println("Enter your name: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next(); // user input for name
        System.out.println("Welcome to the GUESS THE NUMBER Game, " + name + "! \nShall we begin? Type 'Yes' or 'No'.");
        String response = scanner.next(); // user input for Yes/No
        boolean res = response.equals("Yes");

        if (res) {
            Random rand = new Random(); // generates a random number
            int number = rand.nextInt(10) + 1;
            System.out.println("Guess the number between 1 to 10. You have five attempts.");

            int attempt = 0; // initializes attempt to 0

            while (attempt < 5) {
                attempt++;
                System.out.println("Enter your guess: ");
                int answer = scanner.nextInt();

                if (answer == number) {
                    System.out.println("Congratulations! You guessed the random number, which is " + number + ".");
                    break;
                } else if (answer < number) {
                    System.out.println("Higher!");
                } else if (answer > number) {
                    System.out.println("Lower!");
                }
            }

            if (attempt == 5) {
                System.out.println("Game Over! The random number is " + number + ".");
            }

        } else {
            System.out.println("Game Over.");
        }
    }
}

// 2:04:14